{-# LANGUAGE RecordWildCards #-}

module GuchiTx.Borrow.OffChain where

import Data.Text                ( pack )





import GuchiTx.StateMachine     ( MachineTransition (mtMachineWithState) )
import GuchiTx.StateMachine     qualified as SM

import GuchiTx.Borrow.OffTypes
import GuchiTx.Borrow.OnTypes
import GuchiTx.Borrow.Validator ( initValue )
import GuchiTx.Common.OffChain
import GuchiTx.Common.OnTypes
import GuchiTx.Types
    ( CurrentState (CurrentState, csMachineParams, csStateParams, csTxOutRef)
    , Request (..)
    , Response (..)
    , TxError (TransitionError, UtxError)
    )

initTx :: InitCParams -> PrereqParams -> Either TxError BorrowResponse
initTx icp@InitCParams{..} pp =
    let
        ip = ipFromIcp icp
        tt = SM.mkThreadToken (fst icpUtxo)
        trans = SM.initMachine icpUtxo (mkMachineWithMStake $ BorrowParams pp ip tt) Initted (initValue pp ip) Nothing
        bp = BorrowParams pp ip tt
        utxM = SM.transTx trans
    in case utxM of
            Left e    -> Left (UtxError $ pack $ show e)
            Right utx -> Right $ Response bp (SM.State Initted $ initValue pp ip) (modTxOut (modStakeHack isScript (ppSkh pp)) utx)

stepTx :: BorrowRequest -> Either TxError BorrowResponse
stepTx Request{..} =
    let CurrentState{..} = rqCurrent
        mtM = SM.mkStep (mkMachineWithUtxo csTxOutRef csStateParams csMachineParams) rqInput
        BorrowParams pp _ _ = csMachineParams
    in case mtM of
            Left e -> Left  (TransitionError $ pack $ show e)
            Right mt -> do
                case SM.transTx mt of
                     Left e    -> Left (UtxError $ pack $ show e)
                     Right utx -> Right $ Response csMachineParams (SM.mwsState $ mtMachineWithState mt)  (modTxOut (modStakeHack isScript (ppSkh pp)) utx)

