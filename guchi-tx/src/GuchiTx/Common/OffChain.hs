{-# LANGUAGE DataKinds   #-}
{-# LANGUAGE DerivingVia #-}

module GuchiTx.Common.OffChain where




import Ledger                      ( TxOut (txOutAddress) )
import Ledger                      qualified as L
import Plutus.V1.Ledger.Api
    ( Credential (PubKeyCredential)
    , StakingCredential (StakingHash)
    )
import Plutus.V1.Ledger.Api        qualified as L



import Control.Lens                ( Each (each), (%~), (&) )

import Ledger.Constraints          ( UnbalancedTx )
import Ledger.Constraints.OffChain ( tx )
import Ledger.Tx                   ( TxOut (TxOut), outputs )



-- | Intended for external use

skhToCredential :: L.StakePubKeyHash -> L.StakingCredential
skhToCredential (L.StakePubKeyHash pkh) = (StakingHash . PubKeyCredential) pkh

credentialToSkh :: L.StakingCredential -> L.StakePubKeyHash
credentialToSkh (StakingHash (PubKeyCredential pkh)) = L.StakePubKeyHash pkh
credentialToSkh _                                    = error "only key hashes handled"

----------------------------------------------------------------------------------------------------
-- Stake address hack

modTxOut :: (TxOut -> TxOut) -> UnbalancedTx -> UnbalancedTx
modTxOut f utx = utx & tx . outputs . each %~ f

modStakeHack :: (TxOut -> Bool) -> Maybe L.StakePubKeyHash -> TxOut -> TxOut
modStakeHack filt spkh current@(TxOut addr val dat) =
    if filt current
        then TxOut (L.Address pAddr sAddr) val dat
        else current
    where
          L.Address pAddr _ = addr
          sAddr = fmap (L.StakingHash . L.PubKeyCredential . L.unStakePubKeyHash) spkh

isScript :: TxOut -> Bool
isScript txout = case txOutAddress txout of
                      L.Address (L.ScriptCredential _) _ -> True
                      _                                  -> False
