{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE RecordWildCards    #-}
{-# LANGUAGE TemplateHaskell    #-}
{-# LANGUAGE TypeApplications   #-}

module GuchiTx.Common.OnTypes where

import Prelude          qualified as Haskell

import Data.Aeson       ( FromJSON, ToJSON )
import GHC.Generics     ( Generic )

import Ledger           qualified as L
import Ledger.Ada       qualified as Ada
import PlutusTx qualified
import PlutusTx.Prelude ( Integer, Maybe )


newtype LockedCs
  = LockedCs { unLockedCs :: L.CurrencySymbol }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving newtype (FromJSON, ToJSON)

PlutusTx.makeLift ''LockedCs

newtype LockedAmount
  = LockedAmount { unLockedAmount :: Integer }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving newtype (FromJSON, ToJSON)

PlutusTx.makeLift ''LockedAmount

newtype Collateral
  = Collateral { unCollateral :: L.Ada }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving newtype (FromJSON, ToJSON)

PlutusTx.makeLift ''Collateral

newtype LenderFee
  = LenderFee { unLenderFee :: L.Ada }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving newtype (FromJSON, ToJSON)

PlutusTx.makeLift ''LenderFee

newtype FeeCoef
  = FeeCoef { unFeeCoef :: Integer }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving newtype (FromJSON, ToJSON)

PlutusTx.makeLift ''FeeCoef

data PrereqParams
  = PrereqParams
      { ppCancelFee    :: L.Ada
      , ppListingFee   :: L.Ada
      , ppFeeCoef      :: FeeCoef
        -- ^ [--> fee = ppAFeeLinear * (1e-6) * ipFeeA] (Intended use!)
      , ppPkh          :: L.PaymentPubKeyHash
      , ppSkh          :: Maybe L.StakePubKeyHash
        -- ^ Staking address
      , ppMinAdaBuffer :: Integer
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''PrereqParams
