{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NoImplicitPrelude  #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE TemplateHaskell    #-}

{-# OPTIONS_GHC -fno-specialise #-}

module GuchiTx.Hodl.OnTypes where

import PlutusTx qualified
import PlutusTx.Prelude             ( Bool (..), Integer, Maybe, (==) )

import Ledger                       qualified as L

import GuchiTx.StateMachine.OnChain ( StateMachine (..) )
import Prelude                      qualified as Haskell

import GHC.Generics                 ( Generic )

import Data.Aeson                   ( FromJSON, ToJSON )
import GuchiTx.Common.OnTypes       ( Collateral, LockedAmount, LockedCs )
import PlutusTx.Prelude             qualified as PlutusTx.Eq

----------------------------------------------------------------------------------------------------

data InitParams
  = InitParams
      { ipLockedCs   :: LockedCs
      , ipAmount     :: LockedAmount
      , ipCollateral :: Collateral
      , ipReturnPkh  :: L.PaymentPubKeyHash
      , ipReturnSkh  :: Maybe L.StakePubKeyHash
      , ipParty      :: L.PaymentPubKeyHash
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''InitParams

----------------------------------------------------------------------------------------------------

data HodlState
  = Initted
  | Canceled
  | Claimed
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

instance PlutusTx.Eq.Eq HodlState where
    {-# INLINABLE (==) #-}
    Initted == Initted   = True
    Canceled == Canceled = True
    Claimed == Claimed   = True
    _ == _               = False

PlutusTx.makeLift ''HodlState
PlutusTx.unstableMakeIsData ''HodlState

----------------------------------------------------------------------------------------------------

data ClaimParams
  = ClaimParams
      { cpAmounts :: [(L.TokenName, Integer)]
      }
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''ClaimParams
PlutusTx.unstableMakeIsData ''ClaimParams

data HodlInput
  = Cancel
  | Claim ClaimParams
  deriving (Generic, Haskell.Eq, Haskell.Ord, Haskell.Show)
  deriving anyclass (FromJSON, ToJSON)

PlutusTx.makeLift ''HodlInput
PlutusTx.unstableMakeIsData ''HodlInput

----------------------------------------------------------------------------------------------------

type HodlMachine = StateMachine HodlState HodlInput
