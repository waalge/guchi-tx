{-# LANGUAGE DeriveAnyClass #-}

module GuchiTx.Types where

import Data.Aeson           ( FromJSON, ToJSON )
import Data.Text            ( Text )

import Ledger               ( TxOutRef )
import Ledger.Constraints   ( UnbalancedTx )
import PlutusPrelude        ( Generic )

import GuchiTx.StateMachine qualified as SM

data CurrentState mparams state
  = CurrentState
      { csMachineParams :: mparams
      , csStateParams   :: SM.State state
        -- ^ Current State
      , csTxOutRef      :: TxOutRef
        -- ^ Current host utxo
      }
  deriving (FromJSON, Generic, Show, ToJSON)

data Request mparams state input
  = Request
      { rqCurrent :: CurrentState mparams state
      , rqInput   :: input
      }
  deriving (FromJSON, Generic, Show, ToJSON)

data Response mparams state
  = Response
      { rMachineParams :: mparams
      , rStateParams   :: SM.State state
      , rUnbalancedTx  :: UnbalancedTx
      }
  deriving (FromJSON, Generic, Show, ToJSON)

data TxError
  = TransitionError Text
  | UtxError Text
  deriving (FromJSON, Generic, Show, ToJSON)

