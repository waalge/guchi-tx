{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DerivingVia       #-}
{-# LANGUAGE OverloadedStrings #-}


module Spec.GuchiTx.Hodl.OffChain where

import Data.Default                        ( def )
import Data.Monoid                         ( Last (Last) )

import Test.Tasty                          ( TestTree, testGroup )

import PlutusPrelude                       ( (^.) )

import Ledger.Value                        ( Value )

import Control.Monad                       ( void )
import Ledger.Ada                          qualified as Ada
import Plutus.Contract.Test
    ( TracePredicate
    , changeInitialWalletValue
    , checkPredicate
    , defaultCheckOptions
    , emulatorConfig
    , valueAtAddress
    , w1
    , w2
    , walletFundsChange
    , (.&&.)
    )
import Plutus.Contract.Test                qualified as Test
import Plutus.Trace                        ( runEmulatorTraceIO' )
import Plutus.Trace                        qualified as E
import PlutusTx.Monoid                     ( inv )
import Wallet.Emulator.Wallet              qualified as W

import Ledger.Ada                          ( Ada (getLovelace) )

import GuchiTx.Common.OnTypes
import GuchiTx.Hodl.OnTypes


import GuchiTx.Common.OnChain              ( calcPFee )
import Spec.GuchiTx.Hodl.OffChain.Contract
import Spec.Utils.Const

runSimpleComplete :: IO ()
runSimpleComplete = runEmulatorTraceIO' def (setupOptions ^. emulatorConfig ) simpleCompleteX

--- START simple complete ---

simpleCompleteP :: TracePredicate
simpleCompleteP =
    walletFundsChange w1 (larsToken <> inv (Ada.toValue $ unCollateral collateral) )
    .&&. walletFundsChange w2 ((Ada.toValue $ unCollateral collateral) <> inv (feeP <> larsToken))
    .&&. valueAtAddress testnetAddress ((==) feeP)

sellPrice :: Value
sellPrice = Ada.adaValueOf 100

collateral :: Collateral
collateral = Collateral $ Ada.fromValue $ Ada.adaValueOf 150

buyPrice :: Value
buyPrice = Ada.adaValueOf 50

feeA :: Value
feeA = Ada.adaValueOf 10

feeP :: Value
feeP = calcPFee examplePP collateral

setupOptions :: Test.CheckOptions
setupOptions =
    changeInitialWalletValue (W.knownWallet 1) (Ada.adaValueOf 1000 <>)
    $ changeInitialWalletValue (W.knownWallet 2) (larsToken <>)
    defaultCheckOptions

simpleCompleteTest :: TestTree
simpleCompleteTest = Test.checkPredicateOptions setupOptions "simpleComplete" simpleCompleteP simpleCompleteX

simpleCompleteX :: E.EmulatorTrace ()
simpleCompleteX = do
    let iw = InitW
            { iwCs = larsCs
            , iwAmount = 1
            , iwCollateral = (getLovelace . unCollateral) collateral
            }
        cp = ClaimParams
            { cpAmounts = [(larsTn, 1)]
            }
    void $ E.waitNSlots 2
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            _ <- E.activateContractWallet w2 $ stepC res1 (Claim cp)
            void $ E.waitNSlots 2
        _ -> void $ E.waitNSlots 0

--- END simple complete ---

--- START cancel ---

cancelP :: TracePredicate
cancelP =
    walletFundsChange w1 (inv cancelFee )
    .&&. valueAtAddress testnetAddress ((==) cancelFee)

cancelFee :: Value
cancelFee = Ada.toValue (ppCancelFee examplePP)

cancelTest :: TestTree
cancelTest = Test.checkPredicateOptions setupOptions "cancel" cancelP cancelX

cancelX :: E.EmulatorTrace ()
cancelX = do
    let iw = InitW
            { iwCs = larsCs
            , iwAmount = 1
            , iwCollateral = (getLovelace . unCollateral) collateral
            }
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    case ob1 of
        Last (Just res1) -> do
            _h2 <- E.activateContractWallet w1 $ stepC res1 Cancel
            void $ E.waitNSlots 2
        _ -> void $ E.waitNSlots 0

--- END cancel ---

--- START cancel malicious ---

cancelMaliciousP :: TracePredicate
cancelMaliciousP =
    walletFundsChange w1 (inv cancelFee )
    .&&. valueAtAddress testnetAddress ((==) cancelFee)

cancelMaliciousTest :: TestTree
cancelMaliciousTest = Test.checkPredicateOptions setupOptions "cancelMalicious" cancelMaliciousP cancelMaliciousX

cancelMaliciousX :: E.EmulatorTrace ()
cancelMaliciousX = do
    let iw = InitW
            { iwCs = larsCs
            , iwAmount = 1
            , iwCollateral = (getLovelace . unCollateral) collateral
            }
    h1 <- E.activateContractWallet w1 $ initC w1 iw
    void $ E.waitNSlots 2
    ob1 <- E.observableState h1
    void $ E.waitNSlots 1
    case ob1 of
        Last (Just res1) -> do
            _h2 <- E.activateContractWallet w2 $ stepC res1 Cancel -- Should fail
            void $ E.waitNSlots 2
            _h2 <- E.activateContractWallet w1 $ stepC res1 Cancel -- Should Succeed
            void $ E.waitNSlots 2
        _ -> void $ E.waitNSlots 0

--- END cancel malicious ---

--- START ZZZ ---

zzzP :: TracePredicate
zzzP = undefined

zzzX :: E.EmulatorTrace ()
zzzX = undefined

zzzTest :: TestTree
zzzTest = checkPredicate "zzzTest" zzzP zzzX

--- END ZZZ ---

tests :: TestTree
tests =
    testGroup "htl contracts"
    [ simpleCompleteTest
    , cancelTest
    , cancelMaliciousTest
    -- , zzzTest
    ]
