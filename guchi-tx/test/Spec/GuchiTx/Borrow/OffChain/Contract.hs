{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DerivingVia       #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Spec.GuchiTx.Borrow.OffChain.Contract where

import Data.Aeson                   ( FromJSON, ToJSON )
import Data.Monoid                  ( Last (Last) )
import Data.Text                    ( Text, pack )

import PlutusPrelude                ( Generic )

import Ledger.Value                 ( CurrencySymbol, TokenName, Value )

import Plutus.Contract
    ( Contract
    , EmptySchema
    , logError
    , submitUnbalancedTx
    , tell
    , throwError
    , utxosAt
    )

import Data.Map                     qualified as Map
import GuchiTx.StateMachine.OnChain qualified as SM
import Ledger                       qualified as L
import Ledger.Value                 qualified as Value
import Plutus.Contract.Test         qualified as Test
import Wallet.Emulator              ( mockWalletPaymentPubKeyHash )
import Wallet.Emulator.Wallet       ( mockWalletAddress )

import GuchiTx.Borrow.OffChain
import GuchiTx.Borrow.OffTypes
import GuchiTx.Borrow.OnTypes
import GuchiTx.Borrow.Validator
import GuchiTx.Types                ( CurrentState (CurrentState), Request (..), Response (..) )
import Spec.Utils.Const             ( examplePP )
type EmulatorContract = Contract (Last BorrowResponse) EmptySchema Text ()

-- | Find utxo from chain.
-- Assumption : Only one machine per address
nextRequest :: BorrowResponse -> BorrowInput -> Contract w s Text BorrowRequest
nextRequest (Response mp@(BorrowParams pp ip tt) state _utx) input = do
    utxos <- utxosAt (SM.machineAddress $ mkInstance pp ip tt)
    case Map.toList utxos of
         [(oref, _)] -> pure $ Request (CurrentState mp state oref) input
         _           -> throwError "Expected exactly one utxo at address"

balanceAndSubmit :: BorrowResponse -> EmulatorContract
balanceAndSubmit res = do
    let utx = rUnbalancedTx res
    _tx <- submitUnbalancedTx utx
    tell $ Last $ Just res

stepC :: BorrowResponse -> BorrowInput -> EmulatorContract
stepC res input = do
    req <- nextRequest res input
    case stepTx req of
         Left e     -> throwError $ pack $ "Bad step ::" ++ show e
         Right res' -> balanceAndSubmit res'

data InitW
  = InitW
      { iwDuration       :: L.POSIXTime
      , iwCollateral     :: Integer
      , iwFee            :: Integer
      , iwAmount         :: Integer
      , iwCurrencySymbol :: L.CurrencySymbol
      }
  deriving (FromJSON, Generic, Show, ToJSON)

-- Crude functions to extract probable intent.
valueToAmounts :: Value -> CurrencySymbol -> [(TokenName, Integer)]
valueToAmounts v cs = fmap (\(_, tn, amt) -> (tn, amt)) $ filter (\(cs', _, _) -> cs' == cs) $ Value.flattenValue v

initC :: Test.Wallet -> InitW -> EmulatorContract
initC w InitW{..} = do
    let addr = mockWalletAddress w
    utxos <- utxosAt addr
    let icp = InitCParams
                { icpCurrencySymbol = iwCurrencySymbol
                , icpAmount = iwAmount
                , icpDuration = iwDuration
                , icpCollateral = iwCollateral
                , icpFee = iwFee
                , icpReturnPkh =  mockWalletPaymentPubKeyHash w
                , icpReturnSkh = Nothing
                , icpPkh = mockWalletPaymentPubKeyHash w
                , icpUtxo = (head . Map.toList) utxos
                }
        resE = initTx icp examplePP
    case resE of
         Right res -> do
             balanceAndSubmit res
         Left _ -> logError @Text "TxError"
