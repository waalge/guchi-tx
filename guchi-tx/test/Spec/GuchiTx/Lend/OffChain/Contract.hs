{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DerivingVia       #-}
{-# LANGUAGE OverloadedStrings #-}

module Spec.GuchiTx.Lend.OffChain.Contract where

import Data.Aeson                   ( FromJSON, ToJSON )
import Data.Monoid                  ( Last (Last) )
import Data.Text                    ( Text, pack )

import PlutusPrelude                ( Generic )

import Ledger.Value                 ( CurrencySymbol, TokenName, Value )

import Plutus.Contract
    ( Contract
    , EmptySchema
    , logError
    , submitUnbalancedTx
    , tell
    , throwError
    , utxosAt
    )

import Data.Map                     ( Map )
import Data.Map                     qualified as Map
import GuchiTx.StateMachine.OnChain qualified as SM
import Ledger                       qualified as L
import Ledger.Ada                   qualified as Ada
import Ledger.Value                 qualified as Value
import Plutus.Contract.Test         qualified as Test
import PlutusTx.Monoid              ( inv )
import Wallet.Emulator              ( mockWalletPaymentPubKeyHash )
import Wallet.Emulator.Wallet       ( mockWalletAddress )

import GuchiTx.Lend.OffChain
import GuchiTx.Lend.OffTypes
import GuchiTx.Lend.OnTypes
import GuchiTx.Lend.Validator
import GuchiTx.Types                ( CurrentState (CurrentState), Request (..), Response (..) )
import Spec.Utils.Const
-- Wrap the functions up as contracts

type EmulatorContract = Contract (Last LendResponse) EmptySchema Text ()

-- | Find utxo from chain.
-- Assumption : Only one machine per address
nextRequest :: LendResponse -> LendInput -> Contract w s Text LendRequest
nextRequest (Response mp@(LendParams pp ip tt) state _utx) input = do
    utxos <- utxosAt (SM.machineAddress $ mkInstance pp ip tt)
    case Map.toList utxos of
         [(oref, _)] -> pure $ Request (CurrentState mp state oref) input
         _           -> throwError "Expected exactly one utxo at address"

balanceAndSubmit :: LendResponse -> EmulatorContract
balanceAndSubmit res = do
    let utx = rUnbalancedTx res
    _tx <- submitUnbalancedTx utx
    tell $ Last $ Just res

stepC :: LendResponse -> LendInput -> EmulatorContract
stepC res input = do
    req <- nextRequest res input
    case stepTx req of
         Left e     -> throwError $ pack $ "Bad step ::" ++ show e
         Right res' -> balanceAndSubmit res'

data InitW
  = InitW
      { iwDuration   :: L.POSIXTime
      , iwCollateral :: Integer
      , iwFee        :: Integer
      }
  deriving (FromJSON, Generic, Show, ToJSON)

-- Crude functions to extract probable intent.
valueToAmounts :: Value -> CurrencySymbol -> [(TokenName, Integer)]
valueToAmounts v cs = fmap (\(_, tn, amt) -> (tn, amt)) $ filter (\(cs', _, _) -> cs' == cs) $ Value.flattenValue v

utxosToCsAmounts :: Map L.TxOutRef L.ChainIndexTxOut -> (CurrencySymbol, [(TokenName, Integer)])
utxosToCsAmounts utxos =
    let totalNonAda :: Value
        totalNonAda = (\x -> x <> (inv . Ada.toValue . Ada.fromValue) x) $ Map.foldr (\ci val -> val <> L._ciTxOutValue ci) mempty utxos
        cs = ((\(cs', _, _) -> cs') . head . Value.flattenValue) totalNonAda
    in (cs, valueToAmounts totalNonAda cs )

initC :: Test.Wallet -> InitW -> EmulatorContract
initC w iw = do
    let addr = mockWalletAddress w
    utxos <- utxosAt addr
    let (cs, amts) = utxosToCsAmounts utxos
    let icp = InitCParams
                { icpCurrencySymbol = cs
                , icpAmounts = amts
                , icpDuration = iwDuration iw
                , icpCollateral = iwCollateral iw
                , icpFee = iwFee iw
                , icpReturnPkh =  mockWalletPaymentPubKeyHash w
                , icpReturnSkh = Nothing
                , icpPkh = mockWalletPaymentPubKeyHash w
                , icpUtxo = (head . Map.toList) utxos
                }
        resE = initTx icp examplePP
    case resE of
         Right res -> do
             balanceAndSubmit res
         Left _ -> logError @Text "TxError"
